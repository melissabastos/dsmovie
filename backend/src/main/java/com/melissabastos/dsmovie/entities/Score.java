package com.melissabastos.dsmovie.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="tb_score")
public class Score {
	//se devi istanziare la chiave composta per garantire
	//Che l'ogetto sara istanziato
	@EmbeddedId
	private ScorePK id = new ScorePK();
	private Double profit;
	
	public Score(ScorePK id, Double profit) {
		this.id = id;
		this.profit = profit;
	}

	public Score() {
	}
	
	public void setMovie(Movie movie) {
		id.setMovie(movie);
	}

	public void setUser(User user) {
		id.setUser(user);
	}

	
	public ScorePK getId() {
		return id;
	}

	public void setId(ScorePK id) {
		this.id = id;
	}

	public Double getProfit() {
		return profit;
	}

	public void setProfit(Double profit) {
		this.profit = profit;
	}

	@Override
	public String toString() {
		return "Score [id=" + id + ", value=" + profit + "]";
	}
	
	
	
	
	
	
}
