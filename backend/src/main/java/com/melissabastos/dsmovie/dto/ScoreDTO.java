package com.melissabastos.dsmovie.dto;

import com.melissabastos.dsmovie.entities.Score;

public class ScoreDTO {
	private Long movie;
	private String email;
	private Double profit;
	
	public ScoreDTO() {
		super();
	}
	public ScoreDTO(Long movie, String email, Double profit) {
		super();
		this.movie = movie;
		this.email = email;
		this.profit = profit;
	}
	public ScoreDTO(Score score) {
		super();
		this.movie = score.getId().getMovie().getId();
		this.email = score.getId().getUser().getEmail();
		this.profit = score.getProfit();
	}
	
	public Long getMovie() {
		return movie;
	}
	public void setMovie(Long movie) {
		this.movie = movie;
	}
	public String getEmail() {
		return email;
	}
	public void setUser(String email) {
		this.email =email;
	}
	public Double getProfit() {
		return profit;
	}
	public void setProfit(Double profit) {
		this.profit = profit;
	}
	
	
	
	
	
}
