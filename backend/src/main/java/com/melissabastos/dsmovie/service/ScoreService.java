package com.melissabastos.dsmovie.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.melissabastos.dsmovie.dto.MovieDTO;
import com.melissabastos.dsmovie.dto.ScoreDTO;
import com.melissabastos.dsmovie.entities.Movie;
import com.melissabastos.dsmovie.entities.Score;
import com.melissabastos.dsmovie.entities.User;
import com.melissabastos.dsmovie.repositories.MovieRepository;
import com.melissabastos.dsmovie.repositories.ScoreRepository;
import com.melissabastos.dsmovie.repositories.UserRepository;

@Service
public class ScoreService {

	@Autowired
	private ScoreRepository scoreRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private MovieRepository movieRepository;
	
	
	@Transactional
	public MovieDTO saveScore(ScoreDTO dto) {
		User user = userRepository.findByEmail(dto.getEmail()); 
		if(user == null) {
			user = new User();
			user.setEmail(dto.getEmail());
			userRepository.saveAndFlush(user);
		}
		
		Movie movie = movieRepository.findById(dto.getMovie()).get();
		
		Score score = new Score();
		score.setMovie(movie);
		score.setUser(user);
		score.setProfit(dto.getProfit());
		
		score = scoreRepository.saveAndFlush(score);
		
		Double sum = 0.0;
		for(Score s: movie.getScores()) {
			sum += s.getProfit();
		}
		
		double avg = sum / movie.getScores().size();
		
		
		movie.setScore(avg);
		movie.setCount(movie.getScores().size());
		
		movie = movieRepository.saveAndFlush(movie);
		
		return new MovieDTO(movie);
		}
}
