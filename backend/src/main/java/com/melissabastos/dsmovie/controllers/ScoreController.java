package com.melissabastos.dsmovie.controllers;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.melissabastos.dsmovie.dto.MovieDTO;
import com.melissabastos.dsmovie.dto.ScoreDTO;
import com.melissabastos.dsmovie.service.ScoreService;


@RestController
@RequestMapping(value = "/scores")
public class ScoreController {

		
		@Autowired
		private ScoreService service;
		
		@PutMapping
		public MovieDTO saveScore(@RequestBody ScoreDTO sd){
			MovieDTO movieDTO = service.saveScore(sd);
			return movieDTO;
			
		}
		
	
}
